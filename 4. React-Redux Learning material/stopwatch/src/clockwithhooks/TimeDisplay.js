import React from 'react';
import getDisplayFormat from '../DisplayFormat';


function TimeDisplay(props) {
    let time = getDisplayFormat(props.time);
    return (
        <h2 className="time">{time}</h2>
    );
}


export default TimeDisplay;