import React from 'react';
import Lap from "./Lap";
function AllLaps(props) {
    return (
        <div className="laps">
            <table className="table ">
                <thead>
                    <th>No.</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Duration</th>
                </thead>
                <tbody>
                    {
                        props.laps.map((lap, idx) => {
                            return (
                                <Lap key={idx} idx={idx} lap={lap} />
                            )
                        })
                    }
                </tbody>
            </table>

        </div>
    );
}


export default AllLaps;