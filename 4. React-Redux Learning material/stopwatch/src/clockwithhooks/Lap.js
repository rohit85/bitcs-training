import React from 'react';
import getDisplayFormat from '../DisplayFormat';


function Lap(props) {
    return (
        <tr className="lap" key={props.lap.previous}>
            <td>{props.idx}</td>
            <td>{getDisplayFormat(props.lap.previous)}</td>
            <td>{getDisplayFormat(props.lap.now)}</td>
            <td>{getDisplayFormat(props.lap.now - props.lap.previous)}</td>
        </tr>
    );
}


export default Lap;