import React, { useState, useEffect } from 'react';

import TimeDisplay from "./TimeDisplay";
import AllLaps from "./AllLaps"

function ClockWithHook() {
    const [time, setTime] = useState(0);
    const [previous, setPrev] = useState(0);
    const [laps, setLaps] = useState([]);
    const [isOn, toggle] = useState(false);

    useEffect(() => {
        if (isOn)
            setTimeout(() => { setTime(time + 10) }, 10);
    })

    const start = () => {
        toggle(true);

    }
    const stop = () => {
        toggle(false);
    }
    const reset = () => {
        toggle(false);
        setTime(0);
        setLaps([]);
    }
    const lap = () => {
        let myLaps = [...laps,
        { now: time, previous: previous }];
        setPrev(time);
        setLaps(myLaps);
    }
    return (
        <div className="App ">
            <div className="container bg-light border text-center p-3">
                <h1 className="heading m-auto">StopWatch</h1>
                <TimeDisplay time={time} />
                <button className="btn btn-primary m-2" onClick={start}>Start</button>
                <button className="btn btn-danger m-2" onClick={stop}>Stop</button>
                <button className="btn btn-secondary m-2" onClick={reset}>Reset</button>
                <button className="btn btn-success m-2" onClick={lap}>Lap</button>

                <AllLaps laps={laps} />
            </div>

        </div>
    );
}





export default ClockWithHook;