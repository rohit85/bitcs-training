import React, { useEffect } from 'react';

import { useDispatch, useSelector } from "react-redux";
import TimeDisplay from "./TimeDisplay";
import AllLaps from "./AllLaps"

function ClockWithHook() {
    const time = useSelector((state) => state.time);
    const laps = useSelector((state) => state.laps);
    const isOn = useSelector((state) => state.isOn);
    const dispatch = useDispatch();
    useEffect(() => {
        if (isOn)
            setTimeout(() => { dispatch({ type: "INCREMENT" }) }, 10);
    })

    const start = () => {

        dispatch({ type: "START", payload: {} })
    }
    const stop = () => {
        dispatch({ type: "STOP" })
    }
    const reset = () => {
        dispatch({ type: "RESET" })
    }
    const lap = () => {

        dispatch({ type: "LAP" })
    }
    return (
        <div className="App ">
            <div className="container bg-light border text-center p-3">
                <h1 className="heading m-auto">StopWatch</h1>
                <TimeDisplay time={time} />
                <button className="btn btn-primary m-2" onClick={start}>Start</button>
                <button className="btn btn-danger m-2" onClick={stop}>Stop</button>
                <button className="btn btn-secondary m-2" onClick={reset}>Reset</button>
                <button className="btn btn-success m-2" onClick={lap}>Lap</button>

                <AllLaps laps={laps} />
            </div>

        </div>
    );
}





export default ClockWithHook;