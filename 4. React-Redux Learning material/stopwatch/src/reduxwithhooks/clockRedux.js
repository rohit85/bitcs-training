import React from 'react';
import { createStore } from "redux";
import { Provider } from "react-redux";
import ClockWithHook from "./clock";
const initState = {
    time: 0,
    previous: 0,
    laps: [],
    isOn: false
}

const reducer = (state = initState, action) => {

    if (action.type === "INCREMENT") {
        const newState = {
            ...state,
            time: state.time + 10
        };
        return newState;
    }
    if (action.type === "START") {
        const newState = {
            ...state,
            isOn: true
        };
        return newState;
    }
    if (action.type === "STOP") {
        return {
            ...state,
            isOn: false
        }
    }
    if (action.type === "RESET") {
        return initState
    }
    if (action.type === "LAP") {
        if (state.isOn) {
            const laps = [
                ...state.laps,
                {
                    previous: state.previous,
                    now: state.time
                }
            ]
            const newState = {
                ...state,
                laps: laps,
                previous: state.time
            };
            return newState;
        }
    }
    return state;
}

const store = createStore(reducer);

export default function ClockWithHooksRedux(props) {
    return (
        <Provider store={store}>
            <ClockWithHook />
        </Provider>
    )
}
