
import './App.css';
import { Component } from "react";
import SimpleClock from './simpleClock/SimpleClock';
import ClockWithHooksRedux from "./reduxwithhooks/clockRedux";
import ClockRedux from "./redux/clockRedux";
import ClockWithHook from "./clockwithhooks/clock";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <ClockWithHooksRedux />
    );
  }
}
export default App;
