function getDisplayFormat(time) {
    let centiseconds = ("0" + (Math.floor(time / 10) % 100)).slice(-2);
    let second = ("0" + (Math.floor(time / 1000) % 60)).slice(-2);
    let minute = ("0" + (Math.floor(time / 60000) % 60)).slice(-2);
    let hour = ("0" + Math.floor(time / 3600000)).slice(-2);
    return `${hour}:${minute}:${second}:${centiseconds}`

}
export default getDisplayFormat;