import React, { Component } from 'react';
import Lap from "./Lap";
class AllLaps extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <div className="laps">
                <table className="table ">
                    <thead>
                        <th>No.</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>Duration</th>
                    </thead>
                    <tbody>
                        {
                            this.props.laps.map((lap, idx) => {
                                return (
                                    <Lap idx={idx} lap={lap} />
                                )
                            })
                        }
                    </tbody>
                </table>

            </div>
        );
    }
}

export default AllLaps;