import React, { Component } from 'react';

import TimeDisplay from "./TimeDisplay";
import AllLaps from "./AllLaps"
class SimpleClock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 0,
            previous: 0,
            status: "stopped",
            laps: [{
                previous: 0,
                now: 1000
            }],
            interval: null
        }
    }
    increment = () => {
        this.setState({ time: this.state.time + 10 })
    }
    start = () => {
        if (!this.state.interval)
            this.setState({ interval: setInterval(this.increment, 10) })
    }
    stop = () => {
        if (this.state.interval)
            clearInterval(this.state.interval);
        this.setState({ interval: null });
    }
    reset = () => {
        if (this.state.interval)
            clearInterval(this.state.interval);
        this.setState({ interval: null, time: 0, laps: [] });
    }
    lap = () => {
        let laps = this.state.laps;
        let now = this.state.time;
        laps.push({ now: now, previous: this.state.previous });
        this.setState({ laps: laps, previous: now });
    }
    render() {
        return (
            <div className="App ">
                <div className="container bg-light border text-center p-3">
                    <h1 className="heading m-auto">StopWatch</h1>
                    <TimeDisplay time={this.state.time} />
                    <button className="btn btn-primary m-2" onClick={this.start}>Start</button>
                    <button className="btn btn-danger m-2" onClick={this.stop}>Stop</button>
                    <button className="btn btn-secondary m-2" onClick={this.reset}>Reset</button>
                    <button className="btn btn-success m-2" onClick={this.lap}>Lap</button>

                    <AllLaps laps={this.state.laps} />
                </div>

            </div>
        );
    }



}

export default SimpleClock;