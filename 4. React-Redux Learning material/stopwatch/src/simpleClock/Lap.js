import React, { Component } from 'react';
import getDisplayFormat from "../DisplayFormat";
class Lap extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <tr className="lap" key={this.props.lap.previous}>
                <td>{this.props.idx}</td>
                <td>{getDisplayFormat(this.props.lap.previous)}</td>
                <td>{getDisplayFormat(this.props.lap.now)}</td>
                <td>{getDisplayFormat(this.props.lap.now - this.props.lap.previous)}</td>
            </tr>
        );
    }
}

export default Lap;