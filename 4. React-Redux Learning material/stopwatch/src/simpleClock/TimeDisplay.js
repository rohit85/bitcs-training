import React, { Component } from 'react';
import getDisplayFormat from "../DisplayFormat";
class TimeDisplay extends Component {

    render() {
        let time = getDisplayFormat(this.props.time);
        return (
            <h2 className="time">{time}</h2>
        );
    }
}

export default TimeDisplay;