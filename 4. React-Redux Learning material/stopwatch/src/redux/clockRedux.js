import React, { Component } from 'react';
import { createStore } from "redux";
import { Provider } from "react-redux";
import Clock from "./clock";
import rootReducer from "./clockReducer";
const store = createStore(rootReducer);

store.subscribe(() => {
    console.log("updated");
    console.log(store.getState());
})

class ClockRedux extends Component {

    render() {
        return (
            <Provider store={store} >
                <Clock />
            </Provider>
        );
    }
}



export default (ClockRedux);