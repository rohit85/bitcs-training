import React, { Component } from 'react';
import { connect } from "react-redux";
import AllLaps from "../simpleClock/AllLaps";
import TimeDisplay from "../simpleClock/TimeDisplay"
class Clock extends Component {

    start = () => {
        let interval = setInterval(this.props.increment, 10);
        this.props.start(interval);
    }

    render() {
        console.log(this.props);
        return (
            <div className="App ">
                <div className="container bg-light border text-center p-3">
                    <h1 className="heading m-auto">StopWatch</h1>
                    <TimeDisplay time={this.props.time} />
                    <button className="btn btn-primary m-2" onClick={this.start}>Start</button>
                    <button className="btn btn-danger m-2" onClick={this.props.stop}>Stop</button>
                    <button className="btn btn-secondary m-2" onClick={this.props.reset}>Reset</button>
                    <button className="btn btn-success m-2" onClick={this.props.lap}>Lap</button>
                    <AllLaps laps={this.props.laps} />

                </div>

            </div>);
    }
}
const mapStatetoProps = (state) => {
    return {
        time: state.time,
        laps: state.laps
    }
}
const mapDispatchtoProps = (dispatch) => {
    return {
        increment: () => { dispatch({ type: "INCREMENT" }) },
        start: (interval) => { dispatch({ type: "START", payload: { interval: interval } }) },
        stop: () => { dispatch({ type: "STOP" }) },
        reset: () => { dispatch({ type: "RESET" }) },
        lap: () => { dispatch({ type: "LAP" }) }

    }
}
export default connect(mapStatetoProps, mapDispatchtoProps)(Clock);