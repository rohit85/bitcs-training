const initState = {
    previous: 0,
    time: 0,
    laps: [],
    interval: null
}

const rootReducer = (state = initState, action) => {
    console.log(action);
    if (action.type === "INCREMENT") {
        const newState = {
            ...state,
            time: state.time + 10
        };
        return newState;
    }
    if (action.type === "START") {
        const newState = {
            ...state,
            interval: action.payload.interval
        };
        return newState;
    }
    if (action.type === "STOP") {
        if (state.interval) {
            clearInterval(state.interval);
            const newState = {
                ...state,
                interval: null
            };
            return newState;
        }
    }
    if (action.type === "RESET") {
        if (state.interval) {
            clearInterval(state.interval);

            return initState;
        }
        return initState;
    }
    if (action.type === "LAP") {
        if (state.interval) {
            const laps = [
                ...state.laps,
                {
                    previous: state.previous,
                    now: state.time
                }
            ]
            const newState = {
                ...state,
                laps: laps,
                previous: state.time
            };
            return newState;
        }
    }
    return state;
}
export default rootReducer;
