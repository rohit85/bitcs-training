const mongoose = require("mongoose");

//Set up default mongoose connection
var mongoDB = 'mongodb+srv://rohit:hxdqDyKwjRS73l9E@cluster0.bsxly.mongodb.net/Cats?retryWrites=true&w=majority';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

const CatSchema = mongoose.Schema({
    id: String,
    name: String,
    color: String,
    sound: String,
    breed: { type: String, enum: ["Dalmation", "German shehard"] },
    age: Number,
    dob: Date
});
const Cat = mongoose.model("Cat", CatSchema);

module.exports = Cat;

