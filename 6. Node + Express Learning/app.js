
const express = require("express");
const app = express();
const Cat = require("./models/cat.model");
const uuid = require("uuid").v4;
app.use(express.json());

app.get("/cat/:id", (req, res) => {
    let id = req.params.id;
    Cat.find({ id: id }, function (err, data) {
        if (err) {
            res.send("No cats found");
        } else {
            res.json(data);
        }
    })


})

app.post("/create", (req, res) => {
    let catObj = {
        id: uuid(),
        name: req.body["name"],
        color: req.body["color"],
        sound: req.body["sound"],
        breed: req.body["breed"],
        age: req.body["age"],
        dob: req.body["dob"]
    }
    Cat(catObj).save(function (err) {
        if (err) throw err;
        console.log("saved");
        res.json(catObj);
    })

});

app.post("/update/:id", (req, res) => {
    Cat.update({ id: req.params.id }, req.body.data, function (err, data) {
        if (err) res.end(err);
        res.json(data);
    })

});

app.get("/allcats", (req, res) => {
    let page = req.query.page;
    let maxItem = req.query.items;
    console.log(page);
    maxItem = maxItem ? maxItem : 1;
    Cat.find({}, function (err, cats) {
        if (err) res.end(err);
        if (!page) {
            res.json(cats);
            res.end();
        } else
            if ((page - 1) * maxItem >= cats.length) {
                res.send("This page doesn't exist");
            } else {
                let data = cats.slice((page - 1) * maxItem, Math.min(page * maxItem, cats.length));
                res.json(data);
            }

    });



});

app.delete("/delete/:id", (req, res) => {
    Cat.find({ id: req.params.id }).remove(function (err, data) {
        res.json({
            message: "successfully deleted",
            data: data
        })
    })


});

app.get("/search", (req, res) => {
    /**
     Search cats with certain parameters ( like breed, age, color, sound, etc.
    The API should support if I want to search cats with breed X & Y, and color RED & GREEN)
     */
    console.log(req.query);
    let constraints = {
        breed: req.query.breed ? JSON.parse(req.query["breed"]) : [],
        color: req.query.color ? JSON.parse(req.query["color"]) : [],
        sound: req.query.sound ? JSON.parse(req.query["sound"]) : [],
        minAge: req.query.minAge ? req.query.minAge : 0,
        maxAge: req.query.maxAge ? req.query.maxAge : 100,
        minDate: req.query.minDate ? req.query.minDate : "1/1/1900",
        maxDate: req.query.maxDate ? req.query.maxDate : "1/1/2050",
    };
    console.log(constraints);
    Cat.find({
        age: { $gte: constraints.minAge, $lte: constraints.maxAge },
        $and: [
            { dob: { $gte: constraints.minDate, $lte: constraints.maxDate } },

        ]

    }, function (err, data) {
        if (err) res.end(err);
        data = data.filter((cat) => {
            return (constraints.breed.includes(cat.breed) || constraints.breed.length == 0) &&
                (constraints.color.includes(cat.color) || constraints.color.length == 0) &&
                (constraints.sound.includes(cat.sound) || constraints.sound.length == 0)
        })
        res.json(data);
    })

})

app.get("/categories", (req, res) => {
    let breeds = new Set(), colors = new Set(), sounds = new Set();
    Cat.find()
    Cat.find({}, function (err, data) {
        if (err) res.end(err);
        for (let cat of data) {

            breeds.add(cat.breed);
            colors.add(cat.color);
            sounds.add(cat.sound);
        }

        res.json({
            breeds: Array.from(breeds),
            colors: Array.from(colors),
            sounds: Array.from(sounds)
        })
    })
})

app.listen(3000, () => {
    console.log("server started");
})

