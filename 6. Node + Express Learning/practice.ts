export { };
let message: string = "Hello World";

console.log(typeof (message));
console.log(message);

let multi: number | string;
multi = 30;

multi = "hell";

enum Color { red, green, blue };

let c: Color = Color.blue;

console.log(c)

let u: unknown = 67;


function add(a: number = 4, b?: number): number {
    return a + b;
}

console.log(add(5, 9));


let anytype: any;
anytype = 6;

interface Person {
    first: string,
    last?: string
}

let person: Person = {
    first:"rohit",
    last:"jain"
}
console.log(person.first);




