import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsController } from './cats/cats.controller';
import { CatsService } from "./cats/cats.service"
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [UserModule, AuthModule],
  controllers: [AppController, CatsController],
  providers: [AppService, CatsService, UserService, AuthService],
})
export class AppModule { }
