
import { Param, Body, Controller, Get, Post } from '@nestjs/common';
import { CreateCatDto } from "./dto/createCat.dto";
import {CatsService} from "./cats.service";
import {Cat} from "./interfaces/Cat.interface";
@Controller('cats')
export class CatsController {
    constructor(private readonly catsService:CatsService){}


    @Get()
    findAll(): Cat[] {
        return this.catsService.findAll()
    }
    @Get(":id")
    findOne(@Param("id") id: string): any {
        return `one ${id}`;
    }
    @Post()
    create(@Body() createCatDto: CreateCatDto): any {
        return `${createCatDto.age}   ${createCatDto.name}`
    }
}
