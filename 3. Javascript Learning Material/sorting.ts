export { }
enum Sex { M, F, O };
class Person {
    name: string;
    age: number;
    salary: number;
    sex: Sex;

    constructor(name: string, age: number, salary: number, sex: Sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    static compare(a: Person, b: Person, attr: string, order: string): boolean {
        if (order == 'desc') {
            return a[attr] >= b[attr];
        } else {
            return a[attr] < b[attr];
        }
    }
    static sort(arr: Person[], attr: string, order: string): Person[] {
        if (arr.length <= 1) {
            return arr;
        } else {

            let left: Person[] = [];
            let right: Person[] = [];
            let newArray: Person[] = [];
            let pivot: Person = arr.pop();
            let length: number = arr.length;

            for (let i = 0; i < length; i++) {
                if (this.compare(arr[i], pivot, attr, order)) {
                    left.push(arr[i]);
                } else {
                    right.push(arr[i]);
                }
            }

            return newArray.concat(this.sort(left, attr, order), pivot, this.sort(right, attr, order));

        }
    }
}


let arr: Person[] = [
    
    new Person("Sayantan", 21, 10000, Sex.O),
    new Person("Rohit", 21, 12000, Sex.M),
    new Person("Trishant", 21, 0, Sex.M)
];
console.log(arr);
let sortedArr: Person[] = Person.sort(arr, "name", "asc");


console.log(sortedArr);
