"use strict";
exports.__esModule = true;
var Sex;
(function (Sex) {
    Sex[Sex["M"] = 0] = "M";
    Sex[Sex["F"] = 1] = "F";
    Sex[Sex["O"] = 2] = "O";
})(Sex || (Sex = {}));
;
var Person = /** @class */ (function () {
    function Person(name, age, salary, sex) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.sex = sex;
    }
    Person.compare = function (a, b, attr, order) {
        if (order == 'desc') {
            return a[attr] >= b[attr];
        }
        else {
            return a[attr] < b[attr];
        }
    };
    Person.sort = function (arr, attr, order) {
        if (arr.length <= 1) {
            return arr;
        }
        else {
            var left = [];
            var right = [];
            var newArray = [];
            var pivot = arr.pop();
            var length_1 = arr.length;
            for (var i = 0; i < length_1; i++) {
                if (this.compare(arr[i], pivot, attr, order)) {
                    left.push(arr[i]);
                }
                else {
                    right.push(arr[i]);
                }
            }
            return newArray.concat(this.sort(left, attr, order), pivot, this.sort(right, attr, order));
        }
    };
    return Person;
}());
var arr = [
    new Person("Sayantan", 21, 10000, Sex.O),
    new Person("Rohit", 21, 12000, Sex.M),
    new Person("Trishant", 21, 0, Sex.M)
];
console.log(arr);
var sortedArr = Person.sort(arr, "name", "asc");
console.log(sortedArr);
